package com.qq.rnsharesdk;

import android.content.Context;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Callback;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.OnekeyShareTheme;


import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.line.Line;


import com.mob.MobSDK;

/**
 * Created by cc on 2017/1/29.
 */

public class MobLoginModule extends ReactContextBaseJavaModule implements PlatformActionListener {
    private Context mContext;
    private Promise mPromise;

    public MobLoginModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.mContext = reactContext.getApplicationContext();
    }

    @Override
    public String getName() {
        return "MobLogin";
    }

    @Override
    public void initialize() {
        super.initialize();
        //初始化Mob
        MobSDK.init(mContext);
    }

    @ReactMethod
    public void shareLineImage(String text, String imageUrl ){
        Platform platform = ShareSDK.getPlatform(Line.NAME);
        Platform.ShareParams shareParams = new  Platform.ShareParams();
        shareParams.setImagePath(imageUrl);
//        shareParams.setImageUrl(ResourcesManager.getInstace(MobSDK.getContext()).getImageUrl());
//        platform.setPlatformActionListener(platformActionListener);
        platform.share(shareParams);
    }

    @ReactMethod
    public void showShare(String platform, String title, String text, String url, String imageUrl, Callback callback) {

        final OnekeyShare oks = new OnekeyShare();
        //指定分享的平台，如果为空，还是会调用九宫格的平台列表界面
//        if(platform == "Twitter"){
//            shareTwitterImage(text,"");
//            return;
//        }
        if (platform != null) {
            oks.setPlatform(platform);
        }
        oks.setSilent(true);
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(title);
        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
        oks.setTitleUrl(url);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(text);
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        //oks.setImageUrl(imageUrl);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        oks.setImagePath(imageUrl);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl(url);
        // // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        // oks.setComment("我是测试评论文本");
        // // site是分享此内容的网站名称，仅在QQ空间使用
        // oks.setSite("ShareSDK");
        // // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        // oks.setSiteUrl("http://sharesdk.cn");

        //启动分享
        oks.show(mContext, callback);


    }

    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> hashMap) {
        if (action == Platform.ACTION_USER_INFOR) {
            PlatformDb platDB = platform.getDb();
            WritableMap map = Arguments.createMap();
            map.putString("token", platDB.getToken());
            map.putString("user_id", platDB.getUserId());
            map.putString("user_name", platDB.getUserName());
            map.putString("user_gender", platDB.getUserGender());
            map.putString("user_icon", platDB.getUserIcon());
            mPromise.resolve(map);
        }
    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {
        mPromise.reject("LoginError", throwable.getMessage());
    }

    @Override
    public void onCancel(Platform platform, int i) {

    }
}