import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
  NativeModules
} from 'react-native';
console.log(NativeModules)
const {MobLogin} = NativeModules
import Toast, {DURATION} from 'react-native-easy-toast'

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = { shareing: false };

  }
  _onPressShare(type) {
      if(this.state.shareing) true;
     this.setState({
       shareing:true
     })

     let platform = ""
    if(Platform.OS === "ios") {
      switch(type){
         case "wechat":
           platform = 22;
           break;
         case "weibo":
           platform = 1;
           break;
         case "twitter":
           platform = 11;
           break;
         case "facebook":
           platform = 10;
           break;
         case "qq":
           platform = 24;
           break;


      }
    } else {
      switch(type){
         case "wechat":
           platform = "Wechat";
           break;
         case "weibo":
           platform = "SinaWeibo";
           break;
         case "twitter":
           platform = "Twitter";
           break;
         case "facebook":
           platform = "Facebook";
           break;
         case "qq":
           platform = "QQ";
           break;

      }
    }

    MobLogin.showShare(platform  ,'我是标题', '分享什么内容', 'http://f1.sharesdk.cn/', 'http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg',(error, someData) => {
      
      if (error) {
        console.log(error)
        this.refs.toast.show(error);
      } else {
        console.log(someData)
         this.refs.toast.show(someData);
         this.setState({
           shareing:false
         })

      }
    })
  }

  render() {
    return (
      <View style={styles.container}>

        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("wechat")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享微信</Text>

        </TouchableOpacity>

        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("facebook")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享FaceBook</Text>

        </TouchableOpacity>

        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("weibo")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享微博</Text>

        </TouchableOpacity>

        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("twitter")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享Twitter</Text>

        </TouchableOpacity>

        <TouchableOpacity style={styles.login} onPress={() => this._onPressShare("qq")}>
          <Text style={{ fontSize: 18, color: 'black' }}>分享QQ</Text>

        </TouchableOpacity>
        <Toast ref="toast"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  login: {
    width: 150,
    height: 60,
    borderColor: 'red',
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 12,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});